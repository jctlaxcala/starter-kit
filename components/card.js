import React from 'react'
import { Image, Text, View, TouchableOpacity, StyleSheet} from 'react-native'

const Card = (props) => {
  return (
    <View style={styles.card} >
      <View style={styles.containerIconTitle}>
        <View style={styles.icon}>{props.text}{props.icon}</View>
        <Text style={styles.title}>{props.title}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create(
  {
    card: {
      backgroundColor: 'transparent',
      height: 100,
      borderRadius: 10,
      paddingHorizontal: 20,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    containerIconTitle:{
      flexDirection: 'row', 
      alignItems: 'center',
    },
    icon: {
      height: 60,
      width: 60,
      backgroundColor: '#777777',
      marginRight: 20,
      borderRadius: 100
    },
    title: {
      fontWeight: '500',
      fontSize: 24,
      color: 'white'
    },
    gobtn: {
      height: 40,
      width: 40,
      backgroundColor: 'white',
      borderRadius: 100
    }
  }
)


export default Card;
