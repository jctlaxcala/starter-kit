import React from 'react'
import { Image, Text, View, TouchableOpacity, StyleSheet} from 'react-native'

const Card = (props) => {
  return (
    <View style={styles.card} >
        <Image style={styles.imagen} source={props.operando1}></Image>
        <Text style={styles.title}>{props.operator1}</Text>
        <Image style={styles.imagen} source={props.operando2}></Image>
        <Text style={styles.title}>{props.operator2}</Text>
        <Image style={styles.imagen} source={props.result}></Image>
    </View>
  );
}

const styles = StyleSheet.create(
  {
    card: {
      backgroundColor: 'transparent',
      height: 100,
      borderRadius: 10,
      paddingHorizontal: 20,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    icon: {
      height: 60,
      width: 60,
      backgroundColor: '#777777',
      borderRadius: 100
    },
    title: {
      fontWeight: '500',
      fontSize: 60,
      color: 'white'
    },
    imagen:{
      width: 64,
      resizeMode: 'contain'
    },
    gobtn: {
      height: 40,
      width: 40,
      backgroundColor: 'white',
      borderRadius: 100
    }
  }
)


export default Card;
