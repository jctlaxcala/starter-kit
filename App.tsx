import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './screens/home-screen';
import OperatorScreen from './screens/operators-screen'; 
import SumaScreenAR from './screens/suma-screen-ar';
import RestaScreenAR from './screens/screen-resta-ar';
import MultiScreenAR from './screens/screen-multiplicacion-ar';
import DivisionScreenAR from './screens/screen-division-ar'
import NumberScreen from './screens/numbers-screen';
import UnoScreenAR from './screens/uno-screen-ar'
import DosScreenAR from './screens/dos-screen-ar'
import TresScreenAR from './screens/tres-screen-ar'
import CuatroScreenAR from './screens/cuatro-screen-ar'
import CincoScreenAR from './screens/cinco-screen-ar'
import SeisScreenAR from './screens/seis-screen-ar'
import SieteScreenAR from './screens/siete-screen-ar'
import OchoScreenAR from './screens/ocho-screen-ar'
import NueveScreenAR from './screens/nueve-screen-ar'
import DiezScreenAR from './screens/diez-screen-ar'
import ExampleScreen from './screens/examples-screen'
import EjerciciosScreen from './screens/ejercicios-screen'
import ResultOneScreenAR from './screens/result-one-screen'
import ResultTwoScreenAR from './screens/result-two-screen'
import ResultThreeScreenAR from './screens/result-three-screen'
import ResultFourScreenAR from './screens/result-four-screen'

const Stack = createNativeStackNavigator();

export const screens = {
  HOME: 'Operaciones Matematicas',
  OPERATORS: 'Operators',
  NUMBERS: 'Numbers',
  SUMAAR: 'SumaAR',
  RESTA: 'RestaAR',
  MULTIPLICACION: 'MultipAR',
  DIVISION: 'DivisionAR',
  UNO: 'UnoAR',
  DOS: 'DosAR',
  TRES: 'TresAR',
  CUATRO: 'CuatroAR',
  CINCO: 'CincoAR',
  SEIS: 'SeisAR',
  SIETE: 'SieteAR',
  OCHO: 'OchoAR',
  NUEVE: 'NueveAR',
  DIEZ: 'DiezAR',
  EXAMPLES: 'Ejemplos',
  EJERCICIOS: 'Ejercicios',
  RESULTONE: 'ResultOne',
  RESULTTWO: 'ResultTwo',
  RESULTTHREE: 'ResultThree',
  RESULTFOUR: 'ResultFour'
}

const MyStack = ({ initialRouteName }: { initialRouteName: string }) => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={initialRouteName}>
        <Stack.Screen name={screens.HOME} component={ HomeScreen }></Stack.Screen>
        <Stack.Screen name={screens.OPERATORS} component={ OperatorScreen }></Stack.Screen>
        <Stack.Screen name={screens.NUMBERS} component={ NumberScreen }></Stack.Screen>
        <Stack.Screen name={screens.SUMAAR} component={ SumaScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.RESTA} component={ RestaScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.MULTIPLICACION} component={ MultiScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.DIVISION} component={ DivisionScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.UNO} component={ UnoScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.DOS} component={ DosScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.TRES} component={ TresScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.CUATRO} component={ CuatroScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.CINCO} component={ CincoScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.SEIS} component={ SeisScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.SIETE} component={ SieteScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.OCHO} component={ OchoScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.NUEVE} component={ NueveScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.DIEZ} component={ DiezScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.EXAMPLES} component={ ExampleScreen }></Stack.Screen>
        <Stack.Screen name={screens.EJERCICIOS} component={ EjerciciosScreen }></Stack.Screen>
        <Stack.Screen name={screens.RESULTONE} component={ ResultOneScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.RESULTTWO} component={ ResultTwoScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.RESULTTHREE} component={ ResultThreeScreenAR }></Stack.Screen>
        <Stack.Screen name={screens.RESULTFOUR} component={ ResultFourScreenAR }></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

const App = () => {
  return (
    <MyStack initialRouteName={screens.HOME} />
  );
};

export default App;