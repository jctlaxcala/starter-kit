import React from 'react'
import {Text, View, TouchableOpacity} from 'react-native';
import Card from '../components/card';

interface MenuScreenProps {
  navigation: any
}


function OperatorScreen({navigation}: MenuScreenProps) {
  return (
    <View
      style={{
        height: '100%',
        width: '100%',
        paddingHorizontal: 28,
        justifyContent: 'center',
      }}>
      <View style={{height: '80%', justifyContent: 'space-between'}}>
        <TouchableOpacity
          style={{backgroundColor: '#61B6FF', borderRadius: 10}}
          onPress={() => {
            navigation.navigate('SumaAR');
          }}>
          <Card
            title="Suma"
            text={
              <View
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    width: '100%',
                    textAlign: 'center',
                    fontSize: 50,
                    color: 'white',
                  }}>
                  +
                </Text>
              </View>
            }
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#61B6FF', borderRadius: 10}}
          onPress={() => {
            navigation.navigate('RestaAR');
          }}>
          <Card
            title="Resta"
            text={
              <View
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    width: '100%',
                    textAlign: 'center',
                    fontSize: 50,
                    color: 'white',
                  }}>
                  -
                </Text>
              </View>
            }
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#61B6FF', borderRadius: 10}}
          onPress={() => {
            navigation.navigate('MultipAR');
          }}>
          <Card
            title="Multiplicacion"
            text={
              <View
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    width: '100%',
                    textAlign: 'center',
                    fontSize: 50,
                    color: 'white',
                  }}>
                  x
                </Text>
              </View>
            }
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#61B6FF', borderRadius: 10}}
          onPress={() => {
            navigation.navigate('DivisionAR');
          }}>
          <Card
            title="Division"
            text={
              <View
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    width: '100%',
                    textAlign: 'center',
                    fontSize: 50,
                    color: 'white',
                  }}>
                  /
                </Text>
              </View>
            }
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default OperatorScreen;