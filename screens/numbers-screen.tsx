import React from 'react'
import {Text, View, TouchableOpacity, ScrollView, SafeAreaView, StyleSheet, StatusBar } from 'react-native';
import Card from '../components/card';

interface MenuScreenProps {
  navigation: any
}


function NumberScreen({navigation}: MenuScreenProps) {
  return (
    <SafeAreaView style={styles.container}>
    <ScrollView style={styles.scrollView}>
  
      <View style={{height: '80%', justifyContent: 'space-between'}}>
        <TouchableOpacity
          style={{backgroundColor: '#FB5158', borderRadius: 10, marginVertical:10}}
          onPress={() => {
            navigation.navigate('UnoAR');
          }}>
          <Card
            title="UNO"
            text={
              <View
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    width: '100%',
                    textAlign: 'center',
                    fontSize: 50,
                    color: 'white',
                  }}>
                  1
                </Text>
              </View>
            }
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#FB5158', borderRadius: 10 , marginVertical:10}}
          onPress={() => {
            navigation.navigate('DosAR');
          }}>
          <Card
            title="DOS"
            text={
              <View
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    width: '100%',
                    textAlign: 'center',
                    fontSize: 50,
                    color: 'white',
                  }}>
                  2
                </Text>
              </View>
            }
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#FB5158', borderRadius: 10 , marginVertical:10}}
          onPress={() => {
            navigation.navigate('TresAR');
          }}>
          <Card
            title="TRES"
            text={
              <View
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    width: '100%',
                    textAlign: 'center',
                    fontSize: 50,
                    color: 'white',
                  }}>
                  3
                </Text>
              </View>
            }
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#FB5158', borderRadius: 10, marginVertical:10}}
          onPress={() => {
            navigation.navigate('CuatroAR');
          }}>
          <Card
            title="CUATRO"
            text={
              <View
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    width: '100%',
                    textAlign: 'center',
                    fontSize: 50,
                    color: 'white',
                  }}>
                  4
                </Text>
              </View>
            }
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#FB5158', borderRadius: 10, marginVertical:10}}
          onPress={() => {
            navigation.navigate('CincoAR');
          }}>
          <Card
            title="CINCO"
            text={
              <View
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    width: '100%',
                    textAlign: 'center',
                    fontSize: 50,
                    color: 'white',
                  }}>
                  5
                </Text>
              </View>
            }
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#FB5158', borderRadius: 10, marginVertical:10}}
          onPress={() => {
            navigation.navigate('SeisAR');
          }}>
          <Card
            title="SEIS"
            text={
              <View
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    width: '100%',
                    textAlign: 'center',
                    fontSize: 50,
                    color: 'white',
                  }}>
                  6
                </Text>
              </View>
            }
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#FB5158', borderRadius: 10, marginVertical:10}}
          onPress={() => {
            navigation.navigate('SieteAR');
          }}>
          <Card
            title="SIETE"
            text={
              <View
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    width: '100%',
                    textAlign: 'center',
                    fontSize: 50,
                    color: 'white',
                  }}>
                  7
                </Text>
              </View>
            }
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#FB5158', borderRadius: 10, marginVertical:10}}
          onPress={() => {
            navigation.navigate('OchoAR');
          }}>
          <Card
            title="OCHO"
            text={
              <View
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    width: '100%',
                    textAlign: 'center',
                    fontSize: 50,
                    color: 'white',
                  }}>
                  8
                </Text>
              </View>
            }
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#FB5158', borderRadius: 10, marginVertical:10}}
          onPress={() => {
            navigation.navigate('NueveAR');
          }}>
          <Card
            title="NUEVE"
            text={
              <View
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    width: '100%',
                    textAlign: 'center',
                    fontSize: 50,
                    color: 'white',
                  }}>
                  9
                </Text>
              </View>
            }
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#FB5158', borderRadius: 10, marginVertical:10}}
          onPress={() => {
            navigation.navigate('DiezAR');
          }}>
          <Card
            title="DIEZ"
            text={
              <View
                style={{
                  width: '100%',
                  height: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    width: '100%',
                    textAlign: 'center',
                    fontSize: 50,
                    color: 'white',
                  }}>
                  10
                </Text>
              </View>
            }
          />
        </TouchableOpacity>
      </View>
  
    </ScrollView>
  </SafeAreaView>

  );

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    /*paddingTop: StatusBar.currentHeight,*/
  },
  scrollView: {
    paddingHorizontal: 28,
    /*marginHorizontal: 20,*/
  },
  text: {
    fontSize: 42,
  },
  card: {
    margin: 10
  }
});


export default NumberScreen;