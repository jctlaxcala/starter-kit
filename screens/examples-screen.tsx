import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Card from '../components/card-examples';

interface MenuScreenProps {
  navigation: any
}

function ExampleScreen({navigation}: MenuScreenProps) {
  return (
    <View
      style={{
        height: '100%',
        width: '100%',
        paddingHorizontal: 28,
        justifyContent: 'center',
      }}>
      <View style={{height: '80%', justifyContent: 'space-between'}}>
        <TouchableOpacity
          style={{backgroundColor: '#FB9951', borderRadius: 10}}>
          <Card
            operando1={require('../assets/cinco.png')}
            operando2={require('../assets/cinco.png')}
            result={require('../assets/diez.png')}
            operator1={'+'}
            operator2={'='}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#FB9951', borderRadius: 10}}>
          <Card
            operando1={require('../assets/siete.png')}
            operando2={require('../assets/dos.png')}
            result={require('../assets/cinco.png')}
            operator1={'-'}
            operator2={'='}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#FB9951', borderRadius: 10}}>
          <Card
            operando1={require('../assets/tres.png')}
            operando2={require('../assets/dos.png')}
            result={require('../assets/seis.png')}
            operator1={'x'}
            operator2={'='}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#FB9951', borderRadius: 10}}>
          <Card
            operando1={require('../assets/seis.png')}
            operando2={require('../assets/dos.png')}
            result={require('../assets/tres.png')}
            operator1={'/'}
            operator2={'='}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default ExampleScreen;