import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Card from '../components/card-examples';

interface MenuScreenProps {
  navigation: any
}

function EjercicioScreen({navigation}: MenuScreenProps) {
  return (
    <View
      style={{
        height: '100%',
        width: '100%',
        paddingHorizontal: 28,
        justifyContent: 'center',
      }}>
      <View style={{height: '80%', justifyContent: 'space-between'}}>
        <TouchableOpacity
          style={{backgroundColor: '#B16AFF', borderRadius: 10}}
          onPress={() => {
            navigation.navigate('ResultOne');
          }}>
          <Card
            operando1={require('../assets/dos.png')}
            operando2={require('../assets/dos.png')}
            result={require('../assets/interrogation-icon.png')}
            operator1={'+'}
            operator2={'='}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#B16AFF', borderRadius: 10}}
          onPress={() => {
            navigation.navigate('ResultTwo');
          }}
          >
          <Card
            operando1={require('../assets/diez.png')}
            operando2={require('../assets/tres.png')}
            result={require('../assets/interrogation-icon.png')}
            operator1={'-'}
            operator2={'='}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#B16AFF', borderRadius: 10}}
          onPress={() => {
            navigation.navigate('ResultThree');
          }}
          >
          <Card
            operando1={require('../assets/tres.png')}
            operando2={require('../assets/tres.png')}
            result={require('../assets/interrogation-icon.png')}
            operator1={'x'}
            operator2={'='}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{backgroundColor: '#B16AFF', borderRadius: 10}}
          onPress={() => {
            navigation.navigate('ResultFour');
          }}
          >
          <Card
            operando1={require('../assets/diez.png')}
            operando2={require('../assets/dos.png')}
            result={require('../assets/interrogation-icon.png')}
            operator1={'/'}
            operator2={'='}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default EjercicioScreen;