import React from 'react';
import { Text, View,  TouchableOpacity} from 'react-native';

import Card from '../components/card';

interface MenuScreenProps {
  navigation: any
}

function HomeScreen({ navigation }: MenuScreenProps) {
  return (<View style={{/*backgroundColor: 'red',*/ height: '100%', width: '100%', paddingHorizontal: 28, justifyContent: 'center'}}>
  <View style={{height: '80%' /*backgroundColor: 'blue'*/, justifyContent: 'space-between'}}>
    <TouchableOpacity style={{backgroundColor:'#61B6FF', borderRadius:10}} onPress={()=>{navigation.navigate('Operators')}}><Card title="Operadores"/></TouchableOpacity> 
    <TouchableOpacity style={{backgroundColor:'#FB5158', borderRadius:10}} onPress={()=>{navigation.navigate('Numbers')}}><Card title="Numeros" /></TouchableOpacity> 
    <TouchableOpacity style={{backgroundColor:'#FB9951', borderRadius:10}} onPress={()=>{navigation.navigate('Ejemplos')}}><Card title="Ejemplos" /></TouchableOpacity>
    <TouchableOpacity style={{backgroundColor:'#B16AFF', borderRadius:10}} onPress={()=>{navigation.navigate('Ejercicios')}}><Card title="Ejercicios" /></TouchableOpacity>
  </View>
</View>)
}

export default HomeScreen