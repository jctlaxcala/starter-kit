import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
import {
  ViroImage,
  ViroARScene,
  ViroConstants,
  ViroARSceneNavigator
} from '@viro-community/react-viro';

const SceneAR = () => {
  const [text, setText] = useState('preparando...');
  const [widthCorrect, setWidthCorrect] = useState(.6);
  const [positionCorrect, setPositionCorrect] = useState(-.3);
  const [widthIncorrect, setWidthIncorrect] = useState(.6);

  function onInitialized(state, reason) {
    console.log('guncelleme', state, reason);
    if (state === ViroConstants.TRACKING_NORMAL) {
      setText('');
    } else if (state === ViroConstants.TRACKING_NONE) {
      // Handle loss of tracking
    }
  }

  function _onClickOne(position, source){
    console.log(position, source)
    setWidthCorrect(1)
    setPositionCorrect(.2)
    setWidthIncorrect(0)
    console.log('correcto');
  }

  function _onClickTwo(position, source){
    console.log(position, source)
    console.log('incorrecto');
  }
  
  return (
    [<ViroARScene onTrackingUpdated={onInitialized}>
      <ViroImage source={require('../assets/diez.png')}
          position={[0, .4, -1]} scale={[widthIncorrect, widthIncorrect, widthIncorrect]}
          onClick={ _onClickTwo }

      />
      <ViroImage source={require('../assets/nueve.png')}
          position={[0, positionCorrect, -1]} scale={[widthCorrect, widthCorrect, widthCorrect]}
          onClick={ _onClickOne }
      />
    </ViroARScene>]
  );
};

const ResultThreeScreenAR = () => {
  return (
    <ViroARSceneNavigator
      autofocus={true}
      initialScene={{
        scene: SceneAR,
      }}
      style={styles.f1}
    />
  );    
};

export default ResultThreeScreenAR;

var styles = StyleSheet.create({
  f1: {flex: 1},
  helloWorldTextStyle: {
    fontFamily: 'Arial',
    fontSize: 30,
    color: '#4FB1FB',
    textAlignVertical: 'center',
    textAlign: 'center',
  },
});