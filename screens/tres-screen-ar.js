import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
import {
  ViroImage,
  ViroARScene,
  ViroText,
  ViroConstants,
  ViroARSceneNavigator
} from '@viro-community/react-viro';

const SceneAR = () => {
  const [text, setText] = useState('preparando...');

  function onInitialized(state, reason) {
    console.log('guncelleme', state, reason);
    if (state === ViroConstants.TRACKING_NORMAL) {
      setText('');
    } else if (state === ViroConstants.TRACKING_NONE) {
      // Handle loss of tracking
    }
  }

  function _onClick(position, source){
    console.log(position, source)
    console.log('clickeaste');
  }
  
  return (
    [<ViroARScene onTrackingUpdated={onInitialized}>
      <ViroImage source={require('../assets/tres.png')}
          position={[0, .4, -1]} scale={[.6, .6, .6]}
          onClick={ _onClick }
      />
      <ViroText
        text={text}
        scale={[0.5, 0.5, 0.5]}
        position={[0, -.3, -1]}
        style={styles.helloWorldTextStyle}
      />
    </ViroARScene>]
  );
};

const TresScreenAR = () => {
  return (
    <ViroARSceneNavigator
      autofocus={true}
      initialScene={{
        scene: SceneAR,
      }}
      style={styles.f1}
    />
  );    
};

export default TresScreenAR;

var styles = StyleSheet.create({
  f1: {flex: 1},
  helloWorldTextStyle: {
    fontFamily: 'Arial',
    fontSize: 30,
    color: '#4FB1FB',
    textAlignVertical: 'center',
    textAlign: 'center',
  },
});